package com.tamisemi.ffars.model;

import java.util.ArrayList;
import java.util.List;
import android.support.v4.app.Fragment;

/**
 * Created by miki on 7/8/17.
 */

public class Menu {
    public String name;
    public int icon;
    public List<SubMenu> subMenu;
    public Fragment fragment;

    public Menu(String name, int icon, Fragment fragment) {
        this.name = name;
        this.icon = icon;
        this.fragment = fragment;
        this.subMenu = new ArrayList<>();
    }

    public Menu(String name, int icon, ArrayList<SubMenu> subMenu) {
        this.name = name;
        this.icon = icon;
        this.subMenu = new ArrayList<>();
        this.subMenu.addAll(subMenu);
    }

    public static class SubMenu {
        public String name;
        public Fragment fragment;

        public SubMenu(String name, Fragment fragment) {
            this.name = name;
            this.fragment = fragment;
        }
    }
}
