package com.tamisemi.ffars;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.tamisemi.ffars.fragment.CloseFriendFragment;
import com.tamisemi.ffars.fragment.DashboardFragment;
import com.tamisemi.ffars.fragment.NotificationFragment;
import com.tamisemi.ffars.fragment.VeryCloseFriendFragment;
import com.tamisemi.ffars.adapter.MenuAdapter;
import com.tamisemi.ffars.model.Menu;
import com.tamisemi.ffars.misc.SubTitle;
import com.tamisemi.ffars.misc.TitleMenu;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MenuAdapter.MenuItemClickListener{


    Toolbar toolbar;
    DrawerLayout drawer;
    ArrayList<Menu> menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        setToolbar();

        drawer = (DrawerLayout) findViewById(R.id.main_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        setNavigationDrawerMenu();
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setNavigationDrawerMenu() {
        MenuAdapter adapter = new MenuAdapter(this, getMenuList(), this);
        RecyclerView navMenuDrawer = (RecyclerView) findViewById(R.id.main_nav_menu_recyclerview);
        navMenuDrawer.setAdapter(adapter);
        navMenuDrawer.setLayoutManager(new LinearLayoutManager(this));
        navMenuDrawer.setAdapter(adapter);

        /*INITIATE SELECT MENU*/
        adapter.selectedItemParent = menu.get(0).name;
        onMenuItemClick(adapter.selectedItemParent);
        adapter.notifyDataSetChanged();
    }


    private List<TitleMenu> getMenuList() {
        List<TitleMenu> list = new ArrayList<>();

        menu = getNavigationMenu();
        for (int i = 0; i < menu.size(); i++) {
            ArrayList<SubTitle> subMenu = new ArrayList<>();
            if (menu.get(i).subMenu.size() > 0){
                for (int j = 0; j < menu.get(i).subMenu.size(); j++) {
                    subMenu.add(new SubTitle(menu.get(i).subMenu.get(j).name));
                }
            }

            list.add(new TitleMenu(menu.get(i).name, subMenu, menu.get(i).icon));
        }

        return list;
    }

    @Override
    public void onMenuItemClick(String itemString) {
        for (int i = 0; i < menu.size(); i++) {
            if (itemString.equals(menu.get(i).name)){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_content, menu.get(i).fragment)
                        .commit();
                break;
            }else{
                for (int j = 0; j < menu.get(i).subMenu.size(); j++) {
                    if (itemString.equals(menu.get(i).subMenu.get(j).name)){
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.main_content, menu.get(i).subMenu.get(j).fragment)
                                .commit();
                        break;
                    }
                }
            }
        }

        if (drawer != null){
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            drawer.openDrawer(GravityCompat.START);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Menu> getNavigationMenu(){
        ArrayList<Menu> menu = new ArrayList<>();

        menu.add(new Menu("Home", R.drawable.ic_dashboard, DashboardFragment.newInstance("homeParam")));

        menu.add(new Menu("Friend", R.drawable.ic_teman, new ArrayList<Menu.SubMenu>() {{
            add(new Menu.SubMenu("Close", CloseFriendFragment.newInstance("closeFriendParam")));
            add(new Menu.SubMenu("Very Close", VeryCloseFriendFragment.newInstance("veryCloseFriend")));
        }}));

        menu.add(new Menu("Notifications", R.drawable.ic_notifications, NotificationFragment.newInstance("notificationParam")));


        return menu;
    }
}
