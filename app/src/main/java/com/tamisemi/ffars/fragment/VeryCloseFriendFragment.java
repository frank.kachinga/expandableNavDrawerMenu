package com.tamisemi.ffars.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tamisemi.ffars.R;

public class VeryCloseFriendFragment extends Fragment {

    private static final String ARG_PARAM = "";

    public static VeryCloseFriendFragment newInstance() {
        VeryCloseFriendFragment fragment = new VeryCloseFriendFragment();
        return fragment;
    }

    public static VeryCloseFriendFragment newInstance(String param) {
        VeryCloseFriendFragment fragment = new VeryCloseFriendFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, param);
        fragment.setArguments(args);
        return fragment;
    }

    String paramText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            paramText = getArguments().getString(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.very_close_friends_view, container, false);;
        TextView paramView = (TextView) view.findViewById(R.id.param);
        paramView.setText(paramText);

        return view;
    }
}
