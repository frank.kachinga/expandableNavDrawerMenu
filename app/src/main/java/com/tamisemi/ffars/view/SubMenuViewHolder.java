package com.tamisemi.ffars.view;

import android.view.View;
import android.widget.TextView;

import com.tamisemi.ffars.R;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

/**
 * Created by miki on 7/7/17.
 */

public class SubMenuViewHolder extends ChildViewHolder {
    private TextView subTitleTextView;

    public SubMenuViewHolder(View itemView) {
        super(itemView);
        subTitleTextView = (TextView) itemView.findViewById(R.id.main_nav_submenu_item_title);
    }

    public void setSubTitletName(String name) {
        subTitleTextView.setText(name);
    }
}
